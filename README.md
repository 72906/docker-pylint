[![pipeline status](https://gitlab.ics.muni.cz/72906/docker-pylint/badges/master/pipeline.svg)](https://gitlab.ics.muni.cz/72906/docker-pylint/commits/master)

Build Docker image pro testování python projektů pomocí pylint.

## Použití:

Do souboru .gitlab-ci.yml přidejte řádky dle vzoru

### Použití pro Python:

    image: registry.gitlab.ics.muni.cz:443/72906/docker-pylint

    before_script:
      - pip3 install -r requirements.txt
    
    test:
      script:
        - pylint --rcfile=pylintrc -E src/*.py
        
        
### Použití pro Ansible:

    image: registry.gitlab.ics.muni.cz:443/72906/docker-pylint
    
    test:
      script:
        - ansible-lint setup-linux.yml
    