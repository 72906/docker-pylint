FROM debian:buster-slim

RUN apt-get update -qq
RUN apt-get upgrade -qq
RUN apt-get dist-upgrade -qq
RUN apt-get install -qq -y pylint
RUN apt-get install -qq -y python-pip python3-pip
RUN pip install ansible-lint
RUN pip3 install ansible-lint
